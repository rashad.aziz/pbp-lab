from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friends/', friend_list), # friend_list will be called when route lab-1/friends is requested
]
