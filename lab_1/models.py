from django.db import models
from django.utils import timezone

class Friend(models.Model):
    name = models.CharField(max_length=50)
    npm = models.CharField(max_length=10)
    dob = models.DateField(default= timezone.now)
