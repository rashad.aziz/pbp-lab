from django.contrib import admin
from .models import Friend

admin.site.register(Friend) # register Friend model to admin site
