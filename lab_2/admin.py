from django.contrib import admin
from .models import Note

admin.site.register(Note) # register Note model so we can use it in admin page