from django.db import models

class Note(models.Model):
    # declare attributes of Note
    to = models.CharField(max_length=50)
    sender = models.CharField(max_length=50) # cannot use 'from' because it's reserved in python
    title = models.CharField(max_length=50)
    message = models.CharField(max_length=250)