from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index), # lab-2/
    path('xml/', xml), # lab-2/xml
    path('json/', json), # lab-2/json
]
