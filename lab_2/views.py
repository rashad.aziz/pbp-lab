from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note

def index(request):
    notes = Note.objects.all() # load all Note objects
    response = {"notes": notes} # dictionary for response
    return render(request, 'lab2.html', response) # render request with lab2.html template

def xml(request):
    notes = Note.objects.all() # load all Note objects
    data = serializers.serialize('xml', notes) # convert model Objects into XML
    return HttpResponse(data, content_type = "application/xml")

def json(request):
    notes = Note.objects.all() # load all Note objects
    data = serializers.serialize('json', notes) # convert model Object into JSON
    return HttpResponse(data, content_type = "application/json")