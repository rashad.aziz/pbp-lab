from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import FriendForm
from lab_1.models import Friend

@login_required(login_url='/admin/login/') # will require user to be logged in as admin to access this endpoint
def index(request):
    friends = Friend.objects.all()
    context = {'friends':friends}
    return render(request, "lab3_index.html", context)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if request.method == "POST": # if request sent to this endpoint was a POST request
        if form.is_valid():
            form.save()
            return redirect('/lab-3')

    else: # for handling other request, specifically GET
        context = {'form': form}
        return render(request, "lab3_form.html", context)