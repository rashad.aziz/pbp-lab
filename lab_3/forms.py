from django import forms
from django.db.models import fields
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta: # this will create a form using our Model fields, so we set the metadata
        model = Friend
        fields = "__all__"