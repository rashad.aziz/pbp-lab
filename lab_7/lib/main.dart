import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Scaffold(
        body: MyBody(),
      ),
    );
  }
}

class MyBody extends StatelessWidget {
  const MyBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        children: [
          const Positioned(
              child: BackButton(
            color: Colors.red,
          )),
          Positioned(
            child: const Icon(
              Icons.account_circle_sharp,
              color: Colors.red,
              size: 40,
            ),
            top: 5,
            left: size.width * 0.87,
          ),
          Positioned(
            child: Container(
              height: 200,
              width: 250,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/blob.png"), fit: BoxFit.fill)),
            ),
            top: size.height * 0.15,
            left: size.width * 0.2,
          ),
          Positioned(
            child: Container(
              height: 200,
              width: 250,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/food.png"), fit: BoxFit.fill)),
            ),
            top: size.height * 0.15,
            left: size.width * 0.2,
          ),
          Positioned(
              child: Text("Classic Hamburger",
                  style: GoogleFonts.archivoBlack(
                      fontSize: 25,
                      color: Colors.pink[900],
                      fontWeight: FontWeight.bold)),
              top: size.height * 0.5,
              left: 20),
          Positioned(
            child: Text(
              "1 piece",
              style: GoogleFonts.judson(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
            ),
            top: size.height * 0.55,
            left: 20,
          ),
          Positioned(
            child: OrderForm(),
            top: size.height * 0.62,
            left: size.width * 0.05,
          ),
          Positioned(
              child: OutlinedButton(
                onPressed: () => {},
                child: Text(
                  "Add To Cart",
                  style: GoogleFonts.archivoBlack(fontSize: 15),
                ),
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                      const EdgeInsets.only(
                          top: 15, bottom: 15, left: 140, right: 140)),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.pink),
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0))),
                ),
              ),
              top: size.height * 0.75,
              left: 15),
          Positioned(
              child: Container(
                width: size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "INGREDIENTS:",
                      style:
                          GoogleFonts.judson(fontSize: 14, color: Colors.grey),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5, right: 15),
                      child: Text(
                        "Whole Grain Bun, Kobe Beed Patty, Freshly Sliced Tomatoes, Cheddar Cheese",
                        textAlign: TextAlign.left,
                        style: GoogleFonts.archivoBlack(
                            color: Colors.pink[900],
                            fontSize: 12,
                            fontWeight: FontWeight.w100),
                      ),
                    )
                  ],
                ),
              ),
              top: size.height * 0.85,
              left: 20),
        ],
      ),
    );
  }
}

class OrderForm extends StatefulWidget {
  const OrderForm({Key? key}) : super(key: key);

  @override
  _OrderFormState createState() => _OrderFormState();
}

class _OrderFormState extends State<OrderForm> {
  int amount = 1;

  void updateAmount(bool mode) {
    setState(() {
      if (mode) {
        amount += 1;
      } else if (amount > 1) {
        amount -= 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 100),
            width: 150,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(50)),
              border: Border.all(color: Colors.grey),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: IconButton(
                    onPressed: () => updateAmount(false),
                    icon: const Icon(
                      Icons.minimize,
                      size: 15,
                    ),
                  ),
                ),
                Text(
                  "$amount",
                  style: GoogleFonts.archivoBlack(fontSize: 18),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0),
                  child: IconButton(
                    onPressed: () => updateAmount(true),
                    icon: const Icon(
                      Icons.add_sharp,
                      size: 20,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
              child: Row(
            children: [
              Text("\$",
                  style: GoogleFonts.judson(
                      fontSize: 28, color: Colors.pink[900])),
              Text("${amount * 8.50}",
                  style: GoogleFonts.archivoBlack(
                      fontSize: 30, color: Colors.pink))
            ],
          ))
        ],
      ),
    );
  }
}
