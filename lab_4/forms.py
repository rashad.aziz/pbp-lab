from django import forms
from django.forms import TextInput, Textarea
from django.db.models import fields
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta: # this will create a form using our Model fields, so we set the metadata
        model = Note
        fields = "__all__"
        widgets = {
            'message': Textarea(attrs={
                'style': 'width: 350px; height: 100px; padding: 5px;',
                'placeholder': 'type your message here...'
                }),
        }