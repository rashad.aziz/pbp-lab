from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all()
    context = {"notes": notes}
    return render(request, "lab4_index.html", context)

def note_list(request):
    notes = Note.objects.all()
    context = {"notes": notes}
    return render(request, "lab4_note_list.html", context)

def add_note(request):
    form = NoteForm(request.POST or None)
    if request.method == "POST": # if request sent to this endpoint was a POST request
        if form.is_valid():
            form.save()
            return redirect('/lab-4')

    else: # for handling other request, specifically GET
        context = {'form': form}
        return render(request, "lab4_form.html", context)