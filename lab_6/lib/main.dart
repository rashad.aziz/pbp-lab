import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'dart:async';
import 'dart:math';
import 'package:odometer/odometer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.lightBlue[800],
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text(
              "Bansospeduli".toUpperCase(),
              style: TextStyle(fontWeight: FontWeight.w900),
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            // allow overflow to be scrolled
            child: Center(
              child: Column(
                // Column MainAxis is the vertical axis, this will put the content at the top
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image(
                    image: AssetImage("assets/banner.png"),
                  ),
                  OutlinedButton(
                      onPressed: () => {},
                      child: Text(
                        "Register",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      style: OutlinedButton.styleFrom(
                          backgroundColor: Colors.lightBlue,
                          minimumSize: Size(150, 50))),
                  Container(
                    margin: EdgeInsets.all(25),
                    child: DefaultTextStyle(
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.blue,
                          fontWeight: FontWeight.bold),
                      child: AnimatedTextKit(
                        animatedTexts: [
                          TypewriterAnimatedText(
                            "Segera daftar dan berpartisipasi dalam program Bansos COVID-19",
                            cursor: "|",
                            speed: Duration(milliseconds: 50),
                            textAlign: TextAlign.center,
                          )
                        ],
                        repeatForever: true,
                      ),
                    ),
                  ),
                  Divider(
                    indent: 50,
                    endIndent: 50,
                  ),
                  DataContainer(),
                ],
              ),
            ),
          ),
          backgroundColor: Colors.white,
        ));
  }
}

class DataContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(15.0),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10))),
              width: double.infinity,
              child: Text(
                "DATA COVID-19 INDONESIA",
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
            Row(
              children: <Widget>[Data(0), Data(1)],
            ),
            Row(
              children: <Widget>[Data(2), Data(3)],
            ),
          ],
        ));
  }
}

class Data extends StatefulWidget {
  final categories = ["POSITIF", "SEMBUH", "DIRAWAT", "MENINGGAL"];
  int index;

  Data(this.index);
  @override
  State<StatefulWidget> createState() {
    return DataState(categories[this.index]);
  }
}

class DataState extends State<Data> with SingleTickerProviderStateMixin {
  String category;
  DataState(this.category);
  AnimationController? animationController;
  late Animation<OdometerNumber> animation;
  Timer? _interval;
  static Random rng = Random();
  int beginNumber = rng.nextInt(10000);
  int endNumber = rng.nextInt(20000);

  void initState() {
    animationController =
        AnimationController(duration: Duration(seconds: 2), vsync: this);
    animation = OdometerTween(
            begin: OdometerNumber(beginNumber), end: OdometerNumber(endNumber))
        .animate(CurvedAnimation(
            curve: Curves.easeIn, parent: animationController!));
    super.initState();

    _interval =
        Timer.periodic(Duration(seconds: rng.nextInt(5) + 2), (Timer t) {
      setState(() {
        beginNumber = rng.nextInt(20000);
      });
    });
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
    _interval?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      fit: FlexFit.tight,
      child: Column(
        children: [
          Container(
              alignment: Alignment.center,
              height: 85,
              width: double.infinity,
              margin: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: Colors.lightBlue[50],
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    child: Text(
                      category,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.lightBlue),
                    ),
                    margin: EdgeInsets.symmetric(vertical: 15),
                  ),
                  AnimatedSlideOdometerNumber(
                    letterWidth: 12,
                    odometerNumber: OdometerNumber(beginNumber),
                    duration: Duration(milliseconds: 500),
                    numberTextStyle:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              )),
        ],
      ),
    );
  }
}
